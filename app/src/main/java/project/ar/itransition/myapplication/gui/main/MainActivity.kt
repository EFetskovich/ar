package project.ar.itransition.myapplication.gui.main

import android.os.Bundle
import project.ar.itransition.myapplication.R
import project.tess.itransition.myapplication.gui.base.components.BaseActivity

class MainActivity : BaseActivity(), MainActivityContract.ViewHelper {

    private var presenter: MainActivityContract.Presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initComponents()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detach()
    }

    private fun initComponents() {
        initPresenter()
    }

    private fun initPresenter(){
        presenter = MainActivityPresenter()
        presenter?.attach(this)
    }

}
