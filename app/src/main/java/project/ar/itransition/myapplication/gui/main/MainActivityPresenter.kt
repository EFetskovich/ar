package project.ar.itransition.myapplication.gui.main

/**
 * @author e.fetskovich on 10/23/18.
 */
class MainActivityPresenter : MainActivityContract.Presenter {

    private var view: MainActivityContract.ViewHelper? = null

    override fun attach(view: MainActivityContract.ViewHelper) {
        this.view = view
    }

    override fun detach() {
        this.view = null
    }
}