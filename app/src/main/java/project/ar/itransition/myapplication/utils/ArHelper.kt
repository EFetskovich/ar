package project.ar.itransition.myapplication.utils

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import com.google.ar.core.ArCoreApk
import com.google.ar.core.Config
import com.google.ar.core.Session
import com.google.ar.core.exceptions.*
import project.ar.itransition.myapplication.R

/**
 * @author e.fetskovich on 10/23/18.
 */
object ArHelper{

    private val TAG = this.javaClass.simpleName
    private val MIN_OPENGL_VERSION = 3.0

    fun displayError(
        context: Context, errorMsg: String, problem: Throwable?
    ) {
        val tag = context.javaClass.simpleName
        val toastText: String
        if (problem != null && problem.message != null) {
            Log.e(tag, errorMsg, problem)
            toastText = errorMsg + ": " + problem.message
        } else if (problem != null) {
            Log.e(tag, errorMsg, problem)
            toastText = errorMsg
        } else {
            Log.e(tag, errorMsg)
            toastText = errorMsg
        }

        Handler(Looper.getMainLooper())
            .post {
                val toast = Toast.makeText(context, toastText, Toast.LENGTH_LONG)
                toast.setGravity(Gravity.CENTER, 0, 0)
                toast.show()
            }
    }

    @Throws(UnavailableException::class)
    fun createArSession(activity: Activity, installRequested: Boolean): Session? {
        var session: Session? = null
        // if we have the camera permission, create the session
        if (PermissionsHelper.hasCameraPermission(activity)) {
            when (ArCoreApk.getInstance().requestInstall(activity, !installRequested)) {
                ArCoreApk.InstallStatus.INSTALL_REQUESTED -> return null
                ArCoreApk.InstallStatus.INSTALLED -> {
                }
            }
            session = Session(activity)
            // IMPORTANT!!!  ArSceneView requires the `LATEST_CAMERA_IMAGE` non-blocking update mode.
            val config = Config(session)
            config.updateMode = Config.UpdateMode.LATEST_CAMERA_IMAGE
            config.planeFindingMode = Config.PlaneFindingMode.VERTICAL
            session.configure(config)
        }
        return session
    }


    fun handleSessionException(
        activity: Activity, sessionException: UnavailableException
    ) {
        val message: String
        if (sessionException is UnavailableArcoreNotInstalledException) {
            message = activity.getString(R.string.session_install_arcore)
        } else if (sessionException is UnavailableApkTooOldException) {
            message = activity.getString(R.string.session_update_arcore)
        } else if (sessionException is UnavailableSdkTooOldException) {
            message = activity.getString(R.string.session_app_update_arcore)
        } else if (sessionException is UnavailableDeviceNotCompatibleException) {
            message = activity.getString(R.string.session_not_supported_arcore)
        } else {
            message = activity.getString(R.string.session_failed_session_create_arcore)
            Log.e(TAG, "Exception: $sessionException")
        }
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    fun checkIsSupportedDeviceOrFinish(activity: Activity): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Toast.makeText(activity, activity.getString(R.string.sceneform_not_supported), Toast.LENGTH_LONG).show()
            activity.finish()
            return false
        }

        val openGlVersionString = (activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
            .deviceConfigurationInfo
            .glEsVersion
        if (java.lang.Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Toast.makeText(activity, activity.getString(R.string.sceneform_requires_opengl), Toast.LENGTH_LONG)
                .show()
            activity.finish()
            return false
        }
        return true
    }

}