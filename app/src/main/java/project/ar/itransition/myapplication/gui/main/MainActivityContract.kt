package project.ar.itransition.myapplication.gui.main

import project.tess.itransition.myapplication.gui.base.mvp.BasePresenter
import project.tess.itransition.myapplication.gui.base.mvp.BaseView

/**
 * @author e.fetskovich on 10/23/18.
 */
interface MainActivityContract {

    interface ViewHelper : BaseView {

    }

    interface Presenter : BasePresenter<ViewHelper> {

    }

}